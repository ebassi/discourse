ARG BASE_IMAGE_TAG
FROM docker.io/discourse/base:$BASE_IMAGE_TAG

ENV RUBY_ALLOCATOR /usr/lib/libjemalloc.so.1
ENV RAILS_ENV production
ENV DEBIAN_FRONTEND noninteractive 

# Install gettext for envsubst
RUN apt-get update && \
    apt-get install -y gettext && \
    apt clean

RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    mkdir -p /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx && \
    chgrp -R 0 /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx && \
    chmod -R g=u /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx

ENV DISCOURSE_RELEASE=stable RAILS_ROOT=/discourse HOME=/discourse
RUN rm -rf /var/www/discourse && \
    mkdir -p discourse && \
    git clone --single-branch --branch "$DISCOURSE_RELEASE"  https://github.com/discourse/discourse.git /discourse    

ADD config/unicorn.conf.rb /discourse/config/unicorn.conf.rb
ADD config/discourse.conf /tmp/discourse-configmap/discourse.conf
ADD config/sidekiq.yml /tmp/discourse-configmap/sidekiq.yml

ADD config/nginx.conf /etc/nginx/nginx.conf
ADD config/discourse-nginx.conf /etc/nginx/conf.d/nginx.conf

# ImageMagick config
# c.f.: https://github.com/discourse/discourse_docker/commit/7b3d1c513f833a0758ba1f647436514dd365bfd0 
ADD config/policy.xml /usr/local/etc/ImageMagick-7/

WORKDIR $HOME

### Plugins
RUN cd /discourse/plugins/ && \
    git clone --depth=1 https://github.com/jonmbake/discourse-ldap-auth && \
    git clone --depth=1 https://github.com/discourse/discourse-oauth2-basic && \
    git clone --depth=1 https://github.com/discourse/discourse-solved && \
    git clone --depth=1 https://github.com/discourse/discourse-saved-searches.git && \
    git clone --depth=1 https://github.com/discourse/discourse-voting.git && \
    git clone --depth=1 https://github.com/discourse/discourse-categories-suppressed

### Gem installation
RUN bundle config --local deployment true && \
    bundle config --local path ./vendor/bundle && \
    bundle config --local without test development && \
    bundle install --jobs 4 && \
    exec bundle exec rake maxminddb:get && \
    find /discourse/vendor/bundle -name tmp -type d -exec rm -rf {} +

RUN yarn add ember-cli

COPY run-discourse.sh run-nginx.sh .
RUN chmod +x ./run-discourse.sh ./run-nginx.sh && \
    chgrp -R 0 /discourse && chmod -R g=u /discourse && \
    chmod +x /discourse/config/unicorn_launcher

ENTRYPOINT ["./run-discourse.sh"]
