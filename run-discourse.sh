#!/bin/bash

# Previously if unicorn stopped abruptly we could have a situation where pids were left around
# This could lead to Sidekiq not booting, and blocking prometheus with an address bind being used.
# This will ensure that on boot our state is clean.
# For next generation of the infra we can consider set up an emptyDir for cleaning when pod goes down.
/bin/rm -f ${HOME}/tmp/pids/*.pid

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
envsubst < /tmp/discourse-configmap/sidekiq.yml > /discourse/config/sidekiq.yml
echo "--> DONE"

export RAILS_ENV="production"
bundle exec rake db:migrate

if bundle exec rake themes:update assets:precompile;
then
  echo "--> Running Unicorn ..."
  # Run discourse with Unicorn
  exec env LD_PRELOAD=$RUBY_ALLOCATOR thpoff bundle exec config/unicorn_launcher -E production -c config/unicorn.conf.rb
fi
