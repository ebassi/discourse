#!/bin/bash

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
echo "--> DONE"

export RAILS_ENV="production"

# Precompile assets. If succees, proceed with Nginx.
exec nginx -g "daemon off;"
